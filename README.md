# cis-ocrd-data

Data files for ocrd. Uses [git lfs](https://git-lfs.github.com/)

Note: git-lfs support is disabled for now, since the automatic
content-type detection seems broken in git-lfs 2.5.1.
